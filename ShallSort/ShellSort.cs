﻿using System.Diagnostics;

static void ShellSort(double[] array)
{
    int n = array.Length;
    int gap = 257;
    double temp;

    while (gap > 0)
    {
        for (int i = 0; i + gap < n; i++)
        {
            int j = i + gap;
            temp = array[j];

            while (j - gap >= 0 && temp < array[j - gap])
            {
                array[j] = array[j - gap];
                j = j - gap;
            }

            array[j] = temp;
        }

        gap = (gap == 1) ? 0 : (int)(gap / 2.2);
    }
}
static void Print(double[] array)
{
    foreach (double item in array)
    {
        Console.Write($"{item:0.00} ");
    }
    Console.WriteLine();
}

static void Main(string[] args)
{
    Console.WriteLine("Введіть кількість випадкових значень:");
    int count = Convert.ToInt32(Console.ReadLine());
    Random rand = new Random();
    double[] array = new double[count];
    for (int i = 0; i < count; i++)
    {
        array[i] = rand.NextDouble() * 400.0 + 0.000000001;
    }

    Console.WriteLine("Вихідний масив:");
    Print(array);

    Stopwatch stopwatch = new Stopwatch();
    stopwatch.Start();
    ShellSort(array);
    stopwatch.Stop();
    long elapsedTicks = stopwatch.ElapsedTicks;
    long elapsedNanoSeconds = elapsedTicks * (1000000000L / Stopwatch.Frequency);
    Console.WriteLine("Відсортований масив:");
    Print(array);
    Console.WriteLine($"Час виконання сортування: {elapsedNanoSeconds} наносекунд");
}