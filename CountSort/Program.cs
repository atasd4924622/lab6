﻿using System.Diagnostics;

static void CountingSort(double[] array)
{
    int n = array.Length;

    double max = array[0];
    for (int i = 1; i < n; i++)
    {
        if (array[i] > max)
            max = array[i];
    }

    int[] count = new int[(int)max + 1];

    for (int i = 0; i < n; i++)
        count[(int)array[i]]++;

    for (int i = 1; i <= max; i++)
        count[i] += count[i - 1];

    double[] output = new double[n];
    for (int i = n - 1; i >= 0; i--)
    {
        output[count[(int)array[i]] - 1] = array[i];
        count[(int)array[i]]--;
    }

    for (int i = 0; i < n; i++)
        array[i] = output[i];
}
static void Print(double[] array)
{
    foreach (double item in array)
    {
        Console.Write($"{item:0.00} ");
    }
    Console.WriteLine();
}

static void Main(string[] args)
{
    Console.WriteLine("Введіть кількість випадкових значень:");
    int count = Convert.ToInt32(Console.ReadLine());
    Random rand = new Random();
    double[] array = new double[count];
    for (int i = 0; i < count; i++)
    {
        array[i] = rand.NextDouble() * 400.0 + 0.000000001;
    }

    Console.WriteLine("Вихідний масив:");
    Print(array);

    Stopwatch stopwatch = new Stopwatch();
    stopwatch.Start();
    CountingSort(array);
    stopwatch.Stop();
    long elapsedTicks = stopwatch.ElapsedTicks;
    long elapsedNanoSeconds = elapsedTicks * (1000000000L / Stopwatch.Frequency);
    Console.WriteLine("Відсортований масив:");
    Print(array);
    Console.WriteLine($"Час виконання сортування: {elapsedNanoSeconds} наносекунд");
}