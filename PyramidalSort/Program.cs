﻿using System;
using System.Diagnostics;

class Program
{
    static void Heapify(double[] array, int n, int i)
    {
        int largest = i;
        int left = 2 * i + 1;
        int right = 2 * i + 2;

        if (left < n && array[left] > array[largest])
            largest = left;

        if (right < n && array[right] > array[largest])
            largest = right;

        if (largest != i)
        {
            double temp = array[i];
            array[i] = array[largest];
            array[largest] = temp;

            Heapify(array, n, largest);
        }
    }

    static void HeapSort(double[] array)
    {
        int n = array.Length;

        for (int i = n / 2 - 1; i >= 0; i--)
            Heapify(array, n, i);

        for (int i = n - 1; i >= 0; i--)
        {
            double temp = array[0];
            array[0] = array[i];
            array[i] = temp;
            Heapify(array, i, 0);
        }
    }

    static void Print(double[] array)
    {
        foreach (double item in array)
        {
            Console.Write($"{item:0.00} ");
        }
        Console.WriteLine();
    }

    static void Main(string[] args)
    {
        Console.WriteLine("Введіть кількість випадкових значень:");
        int count = Convert.ToInt32(Console.ReadLine());
        Random rand = new Random();
        double[] array = new double[count];
        for (int i = 0; i < count; i++)
        {
            array[i] = rand.NextDouble()* 400.0 + 0.000000001;
        }

        Console.WriteLine("Вихідний масив:");
        Print(array);

        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        HeapSort(array);
        stopwatch.Stop();
        long elapsedTicks = stopwatch.ElapsedTicks;
        long elapsedNanoSeconds = elapsedTicks * (1000000000L / Stopwatch.Frequency);
        Console.WriteLine("Відсортований масив:");
        Print(array);
        Console.WriteLine($"Час виконання сортування: {elapsedNanoSeconds} наносекунд");
    }
}